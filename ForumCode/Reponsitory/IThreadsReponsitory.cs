﻿using ForumCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumCode.Reponsitory
{
   public interface IThreadsReponsitory
  {
    Task<List<Threads>> GetAll();
    Task<List<Threads>> GetByCategory(int id);
    Task<Threads> GetThread(int id);
    void Add(Threads thread);
    void Edit(Threads thread);
    void Remove(Threads thread);
    Task SaveChanges();
  }
}
