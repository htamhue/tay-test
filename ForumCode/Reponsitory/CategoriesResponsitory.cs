﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ForumCode.Models;
using Microsoft.EntityFrameworkCore;

namespace ForumCode.Reponsitory
{
  public class CategoriesResponsitory : ICategoriesReponsitory
  {
    ForumContext _context;
    public CategoriesResponsitory(ForumContext context)
    {
      _context = context;
    }

    public Task<List<Categories>> GetAll() => _context.Categories.ToListAsync();
    public Task<Categories> GetCategory(int id)
      => _context.Categories.SingleOrDefaultAsync(m => m.Id == id);


    public void Add(Categories categories) => _context.Categories.Add(categories);
    public void Edit(Categories categories) => _context.Entry(categories).State = EntityState.Modified;

    public void Remove(Categories categories) => _context.Remove(categories);

    public Task SaveChanges() =>
        _context.SaveChangesAsync();
  }
}