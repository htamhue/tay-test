﻿using ForumCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumCode.Reponsitory
{
  public interface IPostsReponsitory
  {
    Task<List<Posts>> GetAll();
    Task<List<Posts>> GetByThread(int id);
    Task<List<Posts>> GetLatest(int id, int num);
    Task<List<Posts>> GetReply(int id);
    Task<Posts> GetPost(int id);
    void Add(Posts post);
    void Edit(Posts post);
    void Remove(Posts post);
    Task SaveChanges();


  }
}
