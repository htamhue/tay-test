﻿using ForumCode.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumCode.Reponsitory
{
  public class PostsReponsitory : IPostsReponsitory
  {
    ForumContext _context;
    public PostsReponsitory(ForumContext context)
    {
      _context = context;
    }
    public Task<List<Posts>> GetAll() => _context.Posts.Where(p => p.ReplyPostId == null).ToListAsync();

    public Task<List<Posts>> GetByThread(int id) => _context.Posts.Where(p => p.ThreadId == id).ToListAsync();
    public Task<List<Posts>> GetLatest(int id, int num) =>
    _context.Posts.Where(p => p.ReplyPostId == null).Where(p => p.ThreadId == id).OrderByDescending(a => a.Created).Take(num).ToListAsync();


    public Task<List<Posts>> GetReply(int id) =>
   _context.Posts.Where(p => p.ReplyPostId == id).OrderByDescending(a => a.Created).ToListAsync();

    public Task<Posts> GetPost(int id) => _context.Posts.SingleOrDefaultAsync(m => m.Id == id);
    public void Add(Posts post) => _context.Posts.Add(post);
    public void Edit(Posts post) => _context.Entry(post).State = EntityState.Modified;

    public void Remove(Posts post) => _context.Remove(post);

    public Task SaveChanges() =>
        _context.SaveChangesAsync();
  }
}
