﻿using ForumCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumCode.Reponsitory
{
    public interface ICategoriesReponsitory
    {
    Task<List<Categories>> GetAll();
    Task<Categories> GetCategory(int id);
    void Add(Categories thread);
    void Edit(Categories thread);
    void Remove(Categories thread);
    Task SaveChanges();
  }
}
