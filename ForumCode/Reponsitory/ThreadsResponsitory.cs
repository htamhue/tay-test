﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ForumCode.Models;
using Microsoft.EntityFrameworkCore;
using ForumCode.Reponsitory;

namespace ForumCode.Data
{
  public class ThreadsReponsitory : IThreadsReponsitory
  {
    ForumContext _context;
    public ThreadsReponsitory(ForumContext context)
    {
      _context = context;
    }

    public Task<List<Threads>> GetAll() => _context.Threads
      .Include(m => m.Posts)
      .Select(m => new Threads
      {
        Id = m.Id,
        Subject = m.Subject,
        NumberPost = m.Posts.Count,
        LastPost = m.Posts.OrderByDescending(a => a.Created).FirstOrDefault()
      }).ToListAsync();

    public Task<List<Threads>> GetByCategory(int id) => _context.Threads
      .Where(m => m.CategoryId == id)
      .Include(m => m.Posts)
      .Select(m => new Threads
      {
        Id = m.Id,
        Subject = m.Subject,
        NumberPost = m.Posts.Count,
        LastPost = m.Posts.OrderByDescending(a => a.Created).FirstOrDefault()
      }).ToListAsync();

    public Task<Threads> GetThread(int id) => _context.Threads.SingleOrDefaultAsync(m => m.Id == id);

    public void Add(Threads thread) => _context.Threads.Add(thread);

    public void Edit(Threads thread) => _context.Entry(thread).State = EntityState.Modified;

    public void Remove(Threads thread) => _context.Remove(thread);

    public Task SaveChanges() => _context.SaveChangesAsync();


  }
}
