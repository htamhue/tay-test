using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ForumCode.Data;
using ForumCode.Services;
using ForumCode.Models;
using ForumCode.Provider;
using ForumCode.Reponsitory;

namespace ForumCode
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {

      ConfigureDatabase(services);

      services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

      services.AddIdentity<ApplicationUser, IdentityRole>()
          .AddEntityFrameworkStores<ApplicationDbContext>()
          .AddDefaultTokenProviders();

      services.AddMvc()
          .AddRazorPagesOptions(options =>
          {
            options.Conventions.AuthorizeFolder("/Account/Manage");
            options.Conventions.AuthorizePage("/Account/Logout");
          });

      // Register no-op EmailSender used by account confirmation and password reset during development
      // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
      services.AddSingleton<IEmailSender, EmailSender>();


      services.AddAntiforgery(opts => opts.HeaderName = "XSRF-TOKEN");

      services.AddResponseCompression();


      // Add application repositories as scoped dependencies so they are shared per every request.
      services.AddScoped<IPostsReponsitory, PostsReponsitory>();
      services.AddScoped<IThreadsReponsitory, ThreadsReponsitory>();
      services.AddScoped<ICategoriesReponsitory, CategoriesResponsitory>();

      services.AddTransient<IRequestUserProvider, RequestUserProvider>();
    }

    public virtual void ConfigureDatabase(IServiceCollection services)
    {
      services.AddDbContext<ForumContext>(options =>
          options.UseSqlServer(Configuration.GetConnectionString("ForumConnection")));
    }




    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseBrowserLink();
        app.UseDatabaseErrorPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
      }

      app.UseResponseCompression();
      app.UseStaticFiles();

      app.UseAuthentication();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  template: "{controller}/{action=Index}/{id?}");
      });
    }


  }
}
