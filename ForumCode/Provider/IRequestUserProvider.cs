﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumCode.Provider
{
public  interface IRequestUserProvider
  {
    string GetUserId();
  }
}
