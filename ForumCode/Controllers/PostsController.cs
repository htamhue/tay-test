﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ForumCode.Models;
using ForumCode.Reponsitory;
using ForumCode.Provider;
using Microsoft.AspNetCore.Authorization;

namespace ForumCode.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]/[action]")]

  public class PostsController : Controller
  {
    IPostsReponsitory _postsReponsitory;
    IRequestUserProvider _requestUserProvider;

    public PostsController(IPostsReponsitory postsRepository, IRequestUserProvider requestUserProvider)
    {
      _postsReponsitory = postsRepository;
      _requestUserProvider = requestUserProvider;
    }

    // GET: api/Posts
    [HttpGet]

    public async Task<IEnumerable<Posts>> GetPosts()
    {
      return await _postsReponsitory.GetAll();


    }



    // GET: api/Posts/GetPosts/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetPosts([FromRoute] int id)
    {
      var posts = await _postsReponsitory.GetPost(id);

      if (posts == null)
      {
        return NotFound();
      }

      return Ok(posts);
    }


    // GET: api/Posts/GetPosts/5
    [HttpGet("{id}")]
    public async Task<IEnumerable<Posts>> GetReply([FromRoute] int id)
    {
      return await _postsReponsitory.GetReply(id);
    }
    //// GET: api/Posts/GetPostsByThread/5

    [HttpGet("{id}")]
    public async Task<IEnumerable<Posts>> GetPostsByThread([FromRoute] int id)
    { 
      return await _postsReponsitory.GetByThread(id);

    }


    // PUT: api/Posts/5
    [HttpPut()]
    public async Task<IActionResult> PutPosts([FromBody] Posts post)
    {
      if (post == null)
      {
        return NotFound();
      }
      _postsReponsitory.Edit(post);
      await _postsReponsitory.SaveChanges();
      return Ok(post);
    }

    // POST: api/Posts 
    [HttpPost()]
    public async Task<IActionResult> PostPosts([FromBody] Posts post)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      if (!string.IsNullOrEmpty(_requestUserProvider.GetUserId()))
        post.UserId = int.Parse(_requestUserProvider.GetUserId());
      post.Created = DateTime.Now;
      _postsReponsitory.Add(post);
      await _postsReponsitory.SaveChanges();


      return Ok(post);
    }


    // POST: api/Posts 
    [HttpPost()]
    public async Task<IActionResult> ReplyPost([FromRoute] int id, [FromBody] Posts post)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      if (!string.IsNullOrEmpty(_requestUserProvider.GetUserId()))
        post.UserId = int.Parse(_requestUserProvider.GetUserId());
      post.Created = DateTime.Now;
      post.ReplyPostId = id;
      _postsReponsitory.Add(post);
      await _postsReponsitory.SaveChanges();


      return Ok(post);
    }


    // DELETE: api/Posts/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePosts([FromRoute] int id)
    {
      var post = await _postsReponsitory.GetPost(id);
      if (post == null) return NotFound();

      _postsReponsitory.Remove(post);
      await _postsReponsitory.SaveChanges();

      return NoContent();
    }

  }
}