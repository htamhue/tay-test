﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ForumCode.Models;
using ForumCode.Reponsitory;
using ForumCode.Provider;

namespace ForumCode.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class CategoriesController : Controller
  {

    private readonly ICategoriesReponsitory _categoriesRepository;
    private readonly IRequestUserProvider _requestUserProvider;

    public CategoriesController(ICategoriesReponsitory categoriesRepository, IRequestUserProvider requestUserProvider)
    {
      _categoriesRepository = categoriesRepository;
      _requestUserProvider = requestUserProvider;
    }


    // GET: api/Categories
    [HttpGet]
    public async Task<IEnumerable<Categories>> GetCategories()
    {
      return await _categoriesRepository.GetAll();
    }

    // GET: api/Categories/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetCategories([FromRoute] int id)
    {
      var category = await _categoriesRepository.GetCategory(id);
      if (category == null)
      {
        return NotFound();
      }
      return Ok(category);
    }

    // PUT: api/Categories/5
    [HttpPut()]
    public async Task<IActionResult> PutCategories([FromBody] Categories categories)
    {

      if (categories == null)
      {
        return NotFound();
      }
      _categoriesRepository.Edit(categories);
    await  _categoriesRepository.SaveChanges();
      return Ok(categories);
    }

    // POST: api/Categories
    [HttpPost]
    public async Task<IActionResult> PostCategories([FromBody] Categories categories)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      if (!string.IsNullOrEmpty(_requestUserProvider.GetUserId()))
        categories.UserId = int.Parse(_requestUserProvider.GetUserId());
      //categories.Created = DateTime.Now;
      _categoriesRepository.Add(categories);
      await _categoriesRepository.SaveChanges();
      return Ok(categories);
    }

    // DELETE: api/Categories/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCategories([FromRoute] int id)
    {
      var categories = await _categoriesRepository.GetCategory(id);
      if (categories == null) return NotFound();

      _categoriesRepository.Remove(categories);
      await _categoriesRepository.SaveChanges();

      return NoContent();
    }
     
  }
}