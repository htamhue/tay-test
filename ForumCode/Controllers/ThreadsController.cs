﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ForumCode.Models;
using ForumCode.Provider;
using ForumCode.Reponsitory;

namespace ForumCode.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]/[action]")]
  public class ThreadsController : Controller
  {


    private readonly IThreadsReponsitory _threadsRepository;
    private readonly IRequestUserProvider _requestUserProvider;

    public ThreadsController(IThreadsReponsitory threadsRepository, IRequestUserProvider requestUserProvider)
    {
      _threadsRepository = threadsRepository;
      _requestUserProvider = requestUserProvider;
    }

    // GET: api/Threads
    [HttpGet]
    public async Task<IEnumerable<Threads>> GetThreads()
    {
      return await _threadsRepository.GetAll();
    }



    // GET: api/Threads
    [HttpGet("{id}")]
    public async Task<IEnumerable<Threads>> GetThreadsByCategories(int id)
    {
      return await _threadsRepository.GetByCategory(id);
    }

    // GET: api/Threads/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetThreads([FromRoute] int id)
    {
      var thread = await _threadsRepository.GetThread(id);
      if (thread == null)
      {
        return NotFound();
      }
      return Ok(thread);
    }


    // PUT: api/Threads/5
    [HttpPut]
    public async Task<IActionResult> PutThreads([FromBody] Threads threads)
    {
      if (threads == null)
      {
        return NotFound();
      }
      _threadsRepository.Edit(threads);
      await _threadsRepository.SaveChanges();
      return Ok(threads);
    }



    // POST: api/Threads
    [HttpPost]
    public async Task<IActionResult> PostThreads([FromBody] Threads threads)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);
      if (!string.IsNullOrEmpty(_requestUserProvider.GetUserId()))
        threads.UserId = int.Parse(_requestUserProvider.GetUserId());
      threads.Created = DateTime.Now;
      _threadsRepository.Add(threads);
      await _threadsRepository.SaveChanges();
      return Ok(threads);
    }

    // DELETE: api/Threads/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteThreads([FromRoute] int id)
    {
      var thread = await _threadsRepository.GetThread(id);
      if (thread == null) return NotFound();

      _threadsRepository.Remove(thread);
      await _threadsRepository.SaveChanges();

      return NoContent();
    }
  }
}