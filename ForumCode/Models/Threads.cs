﻿using System;
using System.Collections.Generic;

namespace ForumCode.Models
{
    public partial class Threads
    {
        public Threads()
        {
            Posts = new HashSet<Posts>();
        }

        public int Id { get; set; }
        public string Subject { get; set; }
        public DateTime? Created { get; set; }
        public int? UserId { get; set; }
        public int? CategoryId { get; set; }
        public int? LastPostId { get; set; }
        public int? NumberPost { get; set; }

        public Categories Category { get; set; }
        public Posts LastPost { get; set; }
        public Users User { get; set; }
        public ICollection<Posts> Posts { get; set; }
    }
}
