﻿using System;
using System.Collections.Generic;

namespace ForumCode.Models
{
    public partial class Users
    {
        public Users()
        {
            Categories = new HashSet<Categories>();
            Posts = new HashSet<Posts>();
            Threads = new HashSet<Threads>();
        }

        public int Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public DateTime? Created { get; set; }

        public ICollection<Categories> Categories { get; set; }
        public ICollection<Posts> Posts { get; set; }
        public ICollection<Threads> Threads { get; set; }
    }
}
