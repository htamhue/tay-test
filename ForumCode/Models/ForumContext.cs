﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ForumCode.Models
{
    public partial class ForumContext : DbContext
    {
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Posts> Posts { get; set; }
        public virtual DbSet<Threads> Threads { get; set; }
        public virtual DbSet<Users> Users { get; set; }

    public ForumContext(DbContextOptions<ForumContext> options)
 : base(options)
    { }
    //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //        {
    //            if (!optionsBuilder.IsConfigured)
    //            {
    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
    //                optionsBuilder.UseSqlServer(@"Server=DESKTOP-OS4Q93P;Database=Forum;Trusted_Connection=True;");
    //            }
    //        }}

    protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categories>(entity =>
            {
                entity.ToTable("categories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(250);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Categories)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_categories_users");
            });

            modelBuilder.Entity<Posts>(entity =>
            {
                entity.ToTable("posts");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("datetime");

                entity.Property(e => e.PostContent)
                    .HasColumnName("post_content")
                    .HasMaxLength(1000);

                entity.Property(e => e.ReplyPostId).HasColumnName("reply_post_id");

                entity.Property(e => e.ThreadId).HasColumnName("thread_id");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(150);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.ReplyPost)
                    .WithMany(p => p.InverseReplyPost)
                    .HasForeignKey(d => d.ReplyPostId)
                    .HasConstraintName("FK_posts_posts");

                entity.HasOne(d => d.Thread)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.ThreadId)
                    .HasConstraintName("FK_posts_threads");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_posts_users");
            });

            modelBuilder.Entity<Threads>(entity =>
            {
                entity.ToTable("threads");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastPostId).HasColumnName("last_post_id");

                entity.Property(e => e.NumberPost).HasColumnName("number_post");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(150);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Threads)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_threads_categories");

                entity.HasOne(d => d.LastPost)
                    .WithMany(p => p.Threads)
                    .HasForeignKey(d => d.LastPostId)
                    .HasConstraintName("FK_threads_posts");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Threads)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_threads_users");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.FullName)
                    .HasColumnName("full_name")
                    .HasMaxLength(50);

                entity.Property(e => e.HashedPassword)
                    .HasColumnName("hashed_password")
                    .HasMaxLength(50);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);
            });
        }
    }
}
