﻿using System;
using System.Collections.Generic;

namespace ForumCode.Models
{
    public partial class Posts
    {
        public Posts()
        {
            InverseReplyPost = new HashSet<Posts>();
            Threads = new HashSet<Threads>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string PostContent { get; set; }
        public DateTime? Created { get; set; }
        public int? ThreadId { get; set; }
        public int? UserId { get; set; }
        public int? ReplyPostId { get; set; }

        public Posts ReplyPost { get; set; }
        public Threads Thread { get; set; }
        public Users User { get; set; }
        public ICollection<Posts> InverseReplyPost { get; set; }
        public ICollection<Threads> Threads { get; set; }
    }
}
