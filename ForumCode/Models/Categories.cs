﻿using System;
using System.Collections.Generic;

namespace ForumCode.Models
{
    public partial class Categories
    {
        public Categories()
        {
            Threads = new HashSet<Threads>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Created { get; set; }
        public int? UserId { get; set; }

        public Users User { get; set; }
        public ICollection<Threads> Threads { get; set; }
    }
}
