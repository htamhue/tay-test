﻿using ForumCode.Data;
using ForumCode.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ForumCodeIntegrationTest.Data
{
  class PredefinedData
  {


    public static string Password = @"https://localhost:44355/Account/Register";

    public static ApplicationUser[] Profiles = new[]
    {
            new ApplicationUser { Email = "vodangbinhtay@gmail", UserName = "vodangbinhtay@gmail"  },
            new ApplicationUser { Email = "tester@test.com", UserName = "tester@test.com"  },
    };

    public static Posts[] posts = new[]
    {
      new Posts { Id = 111, PostContent = "post content 1" ,  Created = DateTime.Now.Subtract(TimeSpan.FromMinutes(60)),  ThreadId =2 },
       new Posts { Id = 222, PostContent = "post content 2" ,  Created = DateTime.Now.Subtract(TimeSpan.FromMinutes(60)) ,  ThreadId =2},
    };


    public static Threads[] threads = new[]
    {
      new Threads { Id = 111, Subject = "thread subject 1" ,  Created = DateTime.Now.Subtract(TimeSpan.FromMinutes(60)),  CategoryId =2 },
       new Threads { Id = 222, Subject = "thread subject 2" ,  Created = DateTime.Now.Subtract(TimeSpan.FromMinutes(60)) ,  CategoryId =2},
    };


    public static Categories[] categories = new[]
    {
      new Categories { Id = 111, Name = "categories subject 1" ,  },
       new Categories { Id = 222, Name   = "categories subject 2" },
    };


    //public static [] Articlesusse = new[] {
    //        new Article { ArticleId = 111, Title = "Test Article 1", Abstract = "Abstract 1", Contents = "Contents 1", CreatedDate = DateTime.Now.Subtract(TimeSpan.FromMinutes(60)) },
    //        new Article { ArticleId = 222, Title = "Test Article 2", Abstract = "Abstract 2", Contents = "Contents 2", CreatedDate = DateTime.Now }
    //    };
  }
}

