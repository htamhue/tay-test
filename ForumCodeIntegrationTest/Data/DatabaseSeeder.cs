﻿using ForumCode.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ForumCodeIntegrationTest.Data;
using ForumCode.Data;

namespace ForumCodeIntegrationTest.Data
{
  public class DatabaseSeeder
  {
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly ForumContext _context;

    public DatabaseSeeder(ForumContext context, UserManager<ApplicationUser> userManager)
    {
      _context = context;
      _userManager = userManager;
    }

    public async Task Seed()
    {
      // Add all the predefined profiles using the predefined password
      foreach (var user in PredefinedData.Profiles)
      {
        await _userManager.CreateAsync(user, PredefinedData.Password);
        // Set the AuthorId navigation property
        if (user.Email == "vodangbinhtay@gmail.com")
        {
          PredefinedData.posts.ToList().ForEach(a => a.Id = int.Parse(user.Id));
          PredefinedData.threads.ToList().ForEach(a => a.Id = int.Parse(user.Id));
          PredefinedData.categories.ToList().ForEach(a => a.Id = int.Parse(user.Id));
        }
      }

      // Add all the predefined articles
      _context.Posts.AddRange(PredefinedData.posts);
      _context.SaveChanges();
      _context.Threads.AddRange(PredefinedData.threads);
      _context.SaveChanges();
      _context.Categories.AddRange(PredefinedData.categories);
      _context.SaveChanges();
    }
  }
}
