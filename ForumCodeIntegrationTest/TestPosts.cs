using ForumCode.Models;
using ForumCodeIntegrationTest.Data;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ForumCodeIntegrationTest
{
  public class TestPosts : TestFixture
  {
    [Fact]
    public async Task GetPosts_ReturnsPostsList()
    {
      // Act
      var response = await _client.GetAsync("/api/posts/getposts");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var posts = JsonConvert.DeserializeObject<Posts[]>(responseString);
      Assert.NotStrictEqual(PredefinedData.posts, posts);
    }

    [Fact]
    public async Task GetPostsByThread_ReturnsPostsList()
    {
      // Act
      var response = await _client.GetAsync($"/api/posts/GetPostsByThread/{PredefinedData.posts[0].ThreadId}");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var posts = JsonConvert.DeserializeObject<Posts[]>(responseString);
      Assert.NotStrictEqual(PredefinedData.posts, posts);
    }


    [Fact]
    public async Task GetPost_ReturnsSpecifiedPosts()
    {
      // Act
      var response = await _client.GetAsync($"/api/posts/getposts/{PredefinedData.posts[0].Id}");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var post = JsonConvert.DeserializeObject<Posts>(responseString);
      Assert.NotStrictEqual(PredefinedData.posts[0], post);
    }

    [Fact]
    public async Task AddPosts_ReturnsAddedPost()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var post = new Posts {   PostContent = "mock contents" };
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json");
      var response = await _client.PostAsync("/api/posts/postposts", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var addedPost = JsonConvert.DeserializeObject<Posts>(responseString);
      Assert.True(addedPost.Id > 0, "Expected added article to have a valid id");
    }

    [Fact]
    public async Task EditPosts_ReturnsEditPosts()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var post = PredefinedData.posts[0];
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json");
      var response = await _client.PutAsync("/api/Posts/PutPosts", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var editPosts = JsonConvert.DeserializeObject<Posts>(responseString);
      Assert.True(editPosts.Id > 0, "Expected added article to have a valid id");
    }

    [Fact]
    public async Task DeleteConfirmation_RedirectsToList_AfterDeletingPosts()
    {
      // Arrange
      //await EnsureAuthenticationCookie();
      //await EnsureAntiforgeryTokenHeader();

      // Act
      var request = new HttpRequestMessage
      {
        Method = HttpMethod.Delete,
        RequestUri = new Uri($"/api/posts/deleteposts/{PredefinedData.posts[0].Id}", UriKind.Relative),
      };
      var response = await _client.SendAsync(request);

      // Assert
      Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
    }
  }
}
