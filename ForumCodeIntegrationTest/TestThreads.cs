﻿using ForumCode.Models;
using ForumCodeIntegrationTest.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ForumCodeIntegrationTest
{
   public class TestThreads : TestFixture
  {
    [Fact]
    public async Task GetThreads_ReturnsThreadsList()
    {
      // Act
      var response = await _client.GetAsync("/api/threads/GetThreads");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var threads = JsonConvert.DeserializeObject<Threads[]>(responseString);
      Assert.NotStrictEqual(PredefinedData.threads, threads);
    }



    [Fact]
    public async Task GetThreadsByCategories_ReturnsThreadsList()
    {
      // Act
      var response = await _client.GetAsync($"/api/threads/GetThreadsByCategories/{PredefinedData.threads[0].CategoryId}");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var threads = JsonConvert.DeserializeObject<Threads[]>(responseString);
      Assert.NotStrictEqual(PredefinedData.threads, threads);
    }


    [Fact]
    public async Task GetThread_ReturnsSpecifiedThread()
    {
      // Act
      var response = await _client.GetAsync($"/api/threads/GetThreads/{PredefinedData.threads[0].Id}");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var thread = JsonConvert.DeserializeObject<Threads>(responseString);
      Assert.NotStrictEqual(PredefinedData.threads[0], thread);
    }

    [Fact]
    public async Task AddThread_ReturnsAddedThread()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var thread = new Threads { Subject = "mock contents" };
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(thread), Encoding.UTF8, "application/json");
      var response = await _client.PostAsync("/api/threads/PostThreads", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var addedThread = JsonConvert.DeserializeObject<Threads>(responseString);
      Assert.True(addedThread.Id > 0, "Expected added article to have a valid id");
    }


    [Fact]
    public async Task EditThreads_ReturnsEditThreads()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var category = PredefinedData.threads[0];
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
      var response = await _client.PutAsync("/api/Threads/PutThreads", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var editThreads = JsonConvert.DeserializeObject<Threads>(responseString);
      Assert.True(editThreads.Id > 0, "Expected added article to have a valid id");
    }



    [Fact]
    public async Task DeleteConfirmation_RedirectsToList_AfterDeletingPosts()
    {
      // Arrange
      //await EnsureAuthenticationCookie();
      //await EnsureAntiforgeryTokenHeader();

      // Act
      var request = new HttpRequestMessage
      {
        Method = HttpMethod.Delete,
        RequestUri = new Uri($"/api/threads/DeleteThreads/{PredefinedData.posts[0].Id}", UriKind.Relative),
      };
      var response = await _client.SendAsync(request);

      // Assert
      Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
    }
  }
}
