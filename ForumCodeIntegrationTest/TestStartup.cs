﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using ForumCode;
using ForumCode.Models;
using ForumCodeIntegrationTest.Data;

namespace ForumCodeIntegrationTest
{
  public class TestStartup : Startup
  {
    public TestStartup(IConfiguration configuration) : base(configuration)
    {
    }

    public override void ConfigureDatabase(IServiceCollection services)
    {
      // Replace default database connection with In-Memory database
      services.AddDbContext<ForumContext>(options =>
          options.UseInMemoryDatabase("forum_test_db"));

      // Register the database seeder
      services.AddTransient<DatabaseSeeder>();
    }


    public override void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      // Perform all the configuration in the base class
      base.Configure(app, env);

      // Now seed the database
      using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
      {
        var seeder = serviceScope.ServiceProvider.GetService<DatabaseSeeder>();
        seeder.Seed().Wait();
      }
    }
  }
}
