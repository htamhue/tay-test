﻿using ForumCode.Models;
using ForumCodeIntegrationTest.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ForumCodeIntegrationTest
{
  public class TestCategories : TestFixture
  {

    [Fact]
    public async Task GetCategories_ReturnsCategoriesList()
    {
      // Act
      var response = await _client.GetAsync("/api/categories");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var categories = JsonConvert.DeserializeObject<Categories[]>(responseString);
      Assert.NotStrictEqual(PredefinedData.categories, categories);
    }


    [Fact]
    public async Task GetCategory_ReturnsSpecifiedCategory()
    {
      // Act
      var response = await _client.GetAsync($"/api/categories/{PredefinedData.categories[0].Id}");

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var categories = JsonConvert.DeserializeObject<Categories>(responseString);
      Assert.NotStrictEqual(PredefinedData.categories[0], categories);
    }


    [Fact]
    public async Task AddCategory_ReturnsAddedCategory()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var category = new Categories { Name = "mock contents" };
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
      var response = await _client.PostAsync("/api/categories", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var addedCategory = JsonConvert.DeserializeObject<Categories>(responseString);
      Assert.True(addedCategory.Id > 0, "Expected added article to have a valid id");
    }

    [Fact]
    public async Task EditCategory_ReturnsEditCategory()
    {
      // Arrange
      //await EnsureAntiforgeryTokenHeader();
      //await EnsureAuthenticationCookie();

      var category =  PredefinedData.categories[0];
      // Act
      var contents = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
      var response = await _client.PutAsync("/api/categories", contents);

      // Assert
      response.EnsureSuccessStatusCode();
      var responseString = await response.Content.ReadAsStringAsync();
      var editCategory = JsonConvert.DeserializeObject<Categories>(responseString);
      Assert.True(editCategory.Id > 0, "Expected added article to have a valid id");
    }



    [Fact]
    public async Task DeleteConfirmation_RedirectsToList_AfterDeletingCategory()
    {
      // Arrange
      //await EnsureAuthenticationCookie();
      //await EnsureAntiforgeryTokenHeader();

      // Act
      var request = new HttpRequestMessage
      {
        Method = HttpMethod.Delete,
        RequestUri = new Uri($"/api/categories/{PredefinedData.categories[0].Id}", UriKind.Relative),
      };
      var response = await _client.SendAsync(request);

      // Assert
      Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
    }
  }
}
