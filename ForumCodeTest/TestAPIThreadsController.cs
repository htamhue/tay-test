﻿using System;
using Xunit;
using ForumCode.Controllers;
using ForumCode.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;
using ForumCode.Services;
using Microsoft.AspNetCore.Mvc;
using ForumCode.Data;
using ForumCode.Provider;
using ForumCode.Reponsitory;

namespace ForumCodeTest
{
  public class TestAPIThreadsController
  {

    Mock<IThreadsReponsitory> threadRepoMock;
    Mock<IRequestUserProvider> requestUserProviderMock;
    private ThreadsController controller;

    public TestAPIThreadsController()
    {
      threadRepoMock = new Mock<IThreadsReponsitory>();
      requestUserProviderMock = new Mock<IRequestUserProvider>();


      controller = new ThreadsController(threadRepoMock.Object, requestUserProviderMock.Object);
    }


    [Fact]
    public async Task GetThreads_ReturnsThreadsList()
    {
      // Arrange
      var mocThreadsList = new List<Threads>
            {
                new Threads { Subject = "thread subject 1"  },
                new Threads { Subject = "thread subject 2" }
            };
      threadRepoMock.Setup(repo => repo.GetAll()).Returns(Task.FromResult(mocThreadsList));

      // Act
      var result = await controller.GetThreads();

      // Assert
      Assert.Equal(mocThreadsList, result);
    }

    [Fact]
    public async Task GetThreadsTest_ReturnsThreads_WhenThreadsExists()
    {
      // Arrange
      var mockId = 42;
      var mockThreads = new Threads { Subject = "thread subject 1" };
      threadRepoMock.Setup(repo => repo.GetThread(mockId)).Returns(Task.FromResult(mockThreads));

      // Act
      var result = await controller.GetThreads(mockId);

      // Assert
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockThreads, actionResult.Value);
    }

    [Fact]
    public async Task AddThreadsTest_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
      // Arrange
      var mockThreads = new Threads { Subject = "thread subject 1" };
      controller.ModelState.AddModelError("Description", "This field is required");

      // Act
      var result = await controller.PostThreads(mockThreads);

      // Assert
      var actionResult = Assert.IsType<BadRequestObjectResult>(result);
      Assert.Equal(new SerializableError(controller.ModelState), actionResult.Value);
    }



    [Fact]
    public async Task AddThreadsTest_ReturnsThreadsSuccessfullyAdded()
    {
      // Arrange
      var mockThreads = new Threads { Subject = "thread subject 1" };
      threadRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PostThreads(mockThreads);

      // Assert
      threadRepoMock.Verify(repo => repo.Add(mockThreads));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockThreads, actionResult.Value);
    }



    [Fact]
    public async Task AddThreadsTest_SetsUserId_BeforeAddingThreadsToRepository()
    {
      // Arrange
      var mockThreads = new Threads { Subject = "thread subject 1" };
      var mockUId = "123";
      threadRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);
      requestUserProviderMock.Setup(provider => provider.GetUserId()).Returns(mockUId);

      // Act
      var result = await controller.PostThreads(mockThreads);

      // Assert
      threadRepoMock.Verify(repo =>
          repo.Add(It.Is<Threads>(thread =>
              thread == mockThreads
              && thread.UserId == int.Parse(mockUId))));
    }


    [Fact]
    public async Task AddThreadsTest_SetsCreatedDate_BeforeAddingThreadsToRepository()
    {
      // Arrange
      var mockThreads = new Threads { Subject = "thread subject 1" };
      var startTime = DateTime.Now;
      threadRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PostThreads(mockThreads);
      var endTime = DateTime.Now;

      // Assert
      threadRepoMock.Verify(repo =>
          repo.Add(It.Is<Threads>(thread =>
              thread == mockThreads
              && thread.Created >= startTime
              && thread.Created <= endTime)));
    }


    [Fact]
    public async Task EditThreadsTest_ReturnsThreadsSuccessfullyEdit()
    {
      // Arrange
      var mockThreads = new Threads { Subject = "mock put content" };
      threadRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PutThreads(mockThreads);

      // Assert
      threadRepoMock.Verify(repo => repo.Edit(mockThreads));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockThreads, actionResult.Value);
    }
    [Fact]
    public async Task DeleteThreadsTest_ReturnsNotFound_WhenThreadsDoesNorExists()
    {
      // Arrange
      var mockId = 42;
      threadRepoMock.Setup(repo => repo.GetThread(mockId)).Returns(Task.FromResult<Threads>(null));

      // Act
      var result = await controller.DeleteThreads(mockId);

      // Assert
      var viewResult = Assert.IsType<NotFoundResult>(result);
    }



    [Fact]
    public async Task DeleteThreadsTest_ReturnsSuccessCode_AfterRemovingThreadsFromRepository()
    {
      // Arrange
      var mockId = 42;
      var mockThreads = new Threads { Subject = "thread subject 1" };
      threadRepoMock.Setup(repo => repo.GetThread(mockId)).Returns(Task.FromResult(mockThreads));
      threadRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.DeleteThreads(mockId);

      // Assert
      threadRepoMock.Verify(repo => repo.Remove(mockThreads));
      Assert.IsType<NoContentResult>(result);
    }

  }
}
