﻿using ForumCode.Controllers;
using ForumCode.Models;
using ForumCode.Provider;
using ForumCode.Reponsitory;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ForumCodeTest
{
    public class TestAPICategoriesController
    {

    Mock<ICategoriesReponsitory> categoriesRepoMock;
    Mock<IRequestUserProvider> requestUserProviderMock;
    private CategoriesController controller;

    public TestAPICategoriesController()
    {
      categoriesRepoMock = new Mock<ICategoriesReponsitory>();
      requestUserProviderMock = new Mock<IRequestUserProvider>();

      controller = new CategoriesController(categoriesRepoMock.Object, requestUserProviderMock.Object);
    }



    [Fact]
    public async Task GetCategories_ReturnsCategoriesList()
    {
      // Arrange
      var mocCategoriesList = new List<Categories>
            {
                new Categories { Name = "mock Category name 1" },
                new Categories { Name = "mock Category name 2" }
            };
      categoriesRepoMock.Setup(repo => repo.GetAll()).Returns(Task.FromResult(mocCategoriesList));

      // Act
      var result = await controller.GetCategories();

      // Assert
      Assert.Equal(mocCategoriesList, result);
    }


    [Fact]
    public async Task GeCategoriesTest_ReturnsNotFound_WhenCategoriesDoesNorExists()
    {
      // Arrange
      var mockId = 42;
      categoriesRepoMock.Setup(repo => repo.GetCategory(mockId)).Returns(Task.FromResult<Categories>(null));

      // Act
      var result = await controller.GetCategories(mockId);

      // Assert
      var viewResult = Assert.IsType<NotFoundResult>(result);
    }


    [Fact]
    public async Task GetCateogriesTest_ReturnsCategories_WhenCategoriesExists()
    {
      // Arrange
      var mockId = 42;
      var mockCateogories = new Categories { Name = "mock categories" };
      categoriesRepoMock.Setup(repo => repo.GetCategory(mockId)).Returns(Task.FromResult(mockCateogories));

      // Act
      var result = await controller.GetCategories(mockId);

      // Assert
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockCateogories, actionResult.Value);
    }


    [Fact]
    public async Task AddCategpriesTest_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
      // Arrange
      var mockCategories = new Categories { Name = "mock Categories" };
      controller.ModelState.AddModelError("Name", "This field is required");

      // Act
      var result = await controller.PostCategories(mockCategories);

      // Assert
      var actionResult = Assert.IsType<BadRequestObjectResult>(result);
      Assert.Equal(new SerializableError(controller.ModelState), actionResult.Value);
    }


    [Fact]
    public async Task AddCategoriesTest_ReturnsCategoriesSuccessfullyAdded()
    {
      // Arrange
      var mockCategories = new Categories { Name = "mock categories content" };
      categoriesRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PostCategories(mockCategories);

      // Assert
      categoriesRepoMock.Verify(repo => repo.Add(mockCategories));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockCategories, actionResult.Value);
    }


    [Fact]
    public async Task DeleteCategoriesTest_ReturnsNotFound_WhenCategoriesDoesNorExists()
    {
      // Arrange
      var mockId = 42;
      categoriesRepoMock.Setup(repo => repo.GetCategory(mockId)).Returns(Task.FromResult<Categories>(null));

      // Act
      var result = await controller.DeleteCategories(mockId);

      // Assert
      var viewResult = Assert.IsType<NotFoundResult>(result);
    }


    [Fact]
    public async Task EditCategoriesTest_ReturnsCategoriesSuccessfullyEdit()
    {
      // Arrange
      var mockCategories = new Categories { Name = "mock put content" };
      categoriesRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PutCategories(mockCategories);

      // Assert
      categoriesRepoMock.Verify(repo => repo.Edit(mockCategories));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockCategories, actionResult.Value);
    }

    [Fact]
    public async Task DeleteCategoriesTest_ReturnsSuccessCode_AfterRemovingCategoriesFromRepository()
    {
      // Arrange
      var mockId = 42;
      var mockCategories = new Categories { Name = "mock post content" };
      categoriesRepoMock.Setup(repo => repo.GetCategory(mockId)).Returns(Task.FromResult(mockCategories));
      categoriesRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.DeleteCategories(mockId);

      // Assert
      categoriesRepoMock.Verify(repo => repo.Remove(mockCategories));
      Assert.IsType<NoContentResult>(result);
    }




  }
}
