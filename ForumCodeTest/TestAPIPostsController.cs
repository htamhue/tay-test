using System;
using Xunit;
using ForumCode.Controllers;
using ForumCode.Data;
using ForumCode.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;
using ForumCode.Services;
using Microsoft.AspNetCore.Mvc;
using ForumCode.Reponsitory;
using ForumCode.Provider;

namespace ForumCodeTest
{
  public class TestAPIPostsController
  {
    Mock<IPostsReponsitory> postRepoMock;
    Mock<IRequestUserProvider> requestUserProviderMock;
    private PostsController controller;

    public TestAPIPostsController()
    {
      postRepoMock = new Mock<IPostsReponsitory>();
      requestUserProviderMock = new Mock<IRequestUserProvider>();

      controller = new PostsController(postRepoMock.Object, requestUserProviderMock.Object);
    }

    [Fact]
    public async Task GetPosts_ReturnsPostsList()
    {
      // Arrange
      var mocPostsList = new List<Posts>
            {
                new Posts { PostContent = "mock PostContent 1" },
                new Posts { PostContent = "mock PostContent 2" }
            };
      postRepoMock.Setup(repo => repo.GetAll()).Returns(Task.FromResult(mocPostsList));

      // Act
      var result = await controller.GetPosts();

      // Assert
      Assert.Equal(mocPostsList, result);
    }

    [Fact]
    public async Task GetPostsByThread_ReturnPostsList()
    {
      var threadId = 1;
      var mocPostsList = new List<Posts>
            {
                new Posts { PostContent = "mock PostContent 1" },
                new Posts { PostContent = "mock PostContent 2" }
            };
      postRepoMock.Setup(repo => repo.GetByThread(threadId)).Returns(Task.FromResult(mocPostsList));

      // Act
      var result = await controller.GetPostsByThread(threadId);

      // Assert
      Assert.Equal(mocPostsList, result);

    }


    [Fact]
    public async Task GetPostsTest_ReturnsNotFound_WhenPostsDoesNorExists()
    {
      // Arrange
      var mockId = 42;
      postRepoMock.Setup(repo => repo.GetPost(mockId)).Returns(Task.FromResult<Posts>(null));

      // Act
      var result = await controller.GetPosts(mockId);

      // Assert
      var viewResult = Assert.IsType<NotFoundResult>(result);
    }



    [Fact]
    public async Task GetPostsTest_ReturnsPosts_WhenPostsExists()
    {
      // Arrange
      var mockId = 42;
      var mockPosts = new Posts { PostContent = "mock article" };
      postRepoMock.Setup(repo => repo.GetPost(mockId)).Returns(Task.FromResult(mockPosts));

      // Act
      var result = await controller.GetPosts(mockId);

      // Assert
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockPosts, actionResult.Value);
    }
     

    [Fact]
    public async Task GetReplyPostsTest_ReturnsPosts_WhenPostsExists()
    {
      // Arrange
      var mockId = 42;
      var mocPostsList = new List<Posts>
            {
                new Posts { PostContent = "mock PostContent 1" },
                new Posts { PostContent = "mock PostContent 2" }
            };
      postRepoMock.Setup(repo => repo.GetReply(mockId)).Returns(Task.FromResult(mocPostsList));

      // Act
      var result = await controller.GetReply(mockId);

      // Assert

      Assert.Equal(mocPostsList, result);
    }

    [Fact]
    public async Task AddPostTest_ReturnsBadRequest_WhenModelStateIsInvalid()
    {
      // Arrange
      var mockPosts = new Posts { PostContent = "mock posts" };
      controller.ModelState.AddModelError("Description", "This field is required");

      // Act
      var result = await controller.PostPosts(mockPosts);

      // Assert
      var actionResult = Assert.IsType<BadRequestObjectResult>(result);
      Assert.Equal(new SerializableError(controller.ModelState), actionResult.Value);
    }


    [Fact]
    public async Task AddPostsTest_ReturnsPostsSuccessfullyAdded()
    {
      // Arrange
      var mockPosts = new Posts { PostContent = "mock post content" };
      postRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PostPosts(mockPosts);

      // Assert
      postRepoMock.Verify(repo => repo.Add(mockPosts));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockPosts, actionResult.Value);
    }


    [Fact]
    public async Task EditPostsTest_ReturnsPostsSuccessfullyEdit()
    {
      // Arrange
      var mockPosts = new Posts { PostContent = "mock put content" };
      postRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PutPosts(mockPosts);

      // Assert
      postRepoMock.Verify(repo => repo.Edit(mockPosts));
      var actionResult = Assert.IsType<OkObjectResult>(result);
      Assert.Equal(mockPosts, actionResult.Value);
    }

    [Fact]
    public async Task AddPostsTest_SetUserId_BeforeAddingPostsToRepository()
    {
      // Arrange
      var mockPosts = new Posts { PostContent = "mock article" };
      var mockAuthorId = "1";
      postRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);
      requestUserProviderMock.Setup(provider => provider.GetUserId()).Returns(mockAuthorId);

      // Act
      var result = await controller.PostPosts(mockPosts);

      // Assert
      postRepoMock.Verify(repo =>
          repo.Add(It.Is<Posts>(post =>
              post == mockPosts
              && post.UserId == int.Parse(mockAuthorId))));
    }

    [Fact]
    public async Task AddPostsTest_SetsCreatedDate_BeforeAddingPostsToRepository()
    {
      // Arrange
      var mockPosts = new Posts { PostContent = "mock post content" };
      var startTime = DateTime.Now;
      postRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.PostPosts(mockPosts);
      var endTime = DateTime.Now;

      // Assert
      postRepoMock.Verify(repo =>
          repo.Add(It.Is<Posts>(article =>
              article == mockPosts
              && article.Created >= startTime
              && article.Created <= endTime)));
    }


    [Fact]
    public async Task DeletePostsTest_ReturnsNotFound_WhenPostsDoesNorExists()
    {
      // Arrange
      var mockId = 42;
      postRepoMock.Setup(repo => repo.GetPost(mockId)).Returns(Task.FromResult<Posts>(null));

      // Act
      var result = await controller.DeletePosts(mockId);

      // Assert
      var viewResult = Assert.IsType<NotFoundResult>(result);
    }


    [Fact]
    public async Task DeletePostsTest_ReturnsSuccessCode_AfterRemovingPostsFromRepository()
    {
      // Arrange
      var mockId = 42;
      var mockPosts = new Posts { PostContent = "mock post content" };
      postRepoMock.Setup(repo => repo.GetPost(mockId)).Returns(Task.FromResult(mockPosts));
      postRepoMock.Setup(repo => repo.SaveChanges()).Returns(Task.CompletedTask);

      // Act
      var result = await controller.DeletePosts(mockId);

      // Assert
      postRepoMock.Verify(repo => repo.Remove(mockPosts));
      Assert.IsType<NoContentResult>(result);
    }



  }
}
